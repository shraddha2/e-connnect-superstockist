package com.econnect.superstockist;

import android.app.Application;

import com.econnect.superstockist.Receiver.ConnectivityReceiver;
import com.econnect.superstockist.Receiver.ConnectivityReceiver;
import com.google.firebase.analytics.FirebaseAnalytics;

public class EconnectSuperStockists extends Application {

    private static EconnectSuperStockists mInstance;
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    public static synchronized EconnectSuperStockists getInstance() {
        return mInstance;

    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }
}